const { SPLAT } = require('triple-beam');
const { createLogger, format, transports } = require('winston');

const {
  combine, timestamp, label, printf, splat, simple, metadata,
} = format;
const Elasticsearch = require('@elastic/elasticsearch');
const WinstonElasticsearch = require('winston-elasticsearch');
const httpContext = require('express-http-context');

const logLevelEnv = process.env.LOG_LEVEL || 'silly';
const environment = process.env.ENVIRONMENT || 'dev';

const now = (unit) => {
  const hrTime = process.hrtime();

  switch (unit) {
    case 'milli':
      return hrTime[0] * 1000 + hrTime[1] / 1000000;

    case 'micro':
      return hrTime[0] * 1000000 + hrTime[1] / 1000;

    case 'nano':
      return hrTime[0] * 1000000000 + hrTime[1];

    default:
      return now('nano');
  }
};

const getTraceId = () => {
  const reqId = httpContext.get('requestId');
  return reqId || 'noTraceId';
};

const getResponseTime = () => {
  const responseTime = httpContext.get('responseTime');
  const totalResponseTime = httpContext.get('totalResponseTime');

  if (typeof totalResponseTime === 'string') return totalResponseTime;
  if (typeof responseTime === 'string') return responseTime;
  if (typeof totalResponseTime === 'bigint' || typeof responseTime === 'bigint') { return ''; }
};

const isJson = (item) => {
  item = typeof item !== 'string' ? JSON.stringify(item) : item;

  try {
    item = JSON.parse(item);
  } catch (e) {
    return false;
  }

  if (typeof item === 'object' && item !== null) {
    return true;
  }

  return false;
};

const myFormatJson = (info) => {
  const data = {
    nanoTimeStamp: now('nano'),
    timeStamp: info.timestamp,
    traceId: getTraceId(),
    serviceName: info.serviceName,
    logLevel: info.level,
    logMessage: info.logMessage,
  };

  const responseTime = getResponseTime();
  if (responseTime) {
    data.responseTime = `${responseTime} ms`;
  }

  if (info[SPLAT] !== undefined && info[SPLAT].length > 0) {
    if (info[SPLAT].length == 1) {
      if (typeof info[SPLAT][0] === TypeError) {
        data.params = info[SPLAT][0].toString();
      } else if (Array.isArray(info[SPLAT][0])) {
        const list = [];
        info[SPLAT][0].forEach((re) => {
          if (re !== undefined && re !== '') {
            list.push(`${JSON.stringify(re).replace(/\"/g, "'")}`);
          }
        });
        data.params = list.join(',');
      } else if (isJson(info[SPLAT][0])) {
        data.params = `${JSON.stringify(info[SPLAT][0]).replace(/\"/g, "'")}`;
      } else {
        data.params = info[SPLAT][0];
      }
    } else if (isJson(info[SPLAT])) {
      data.params = `${JSON.stringify(info[SPLAT]).replace(/\"/g, "'")}`;
    } else {
      data.params = info[SPLAT];
    }
  }
  return data;
};

// decorate log format
const myPrintFormat = printf((info) => {
  if (process.env.LOG_JSON == 'true' || process.env.LOG_JSON === undefined) {
    return JSON.stringify(info);
  }
  return (
    `${info.timeStamp} [${info.serviceName}] [${getTraceId()}] [${
      info.logLevel
    }] ${info.responseTime} ${
      info.logMessage !== undefined && info.logMessage !== ''
        ? `${info.logMessage} `
        : ''
    }${info.params !== undefined && info.params !== ''
      ? `params:${info.params}`
      : ''}`
  );
});

// extracts message and params from logger input
const extractMessageAndParametersWrapper = original => (...args) => {
  if (httpContext.get('loggme') === false) {
    return;
  }

  if (args.length >= 1) {
    args.logMessage = args[0];
  }
  if (args.length > 1) {
    original(args[0], args.slice(1));
  } else {
    original(args[0]);
  }
};

const Logger = async (elasticsearchURI, serviceName) => {
  const options = {
    console: {
      level: logLevelEnv,
      format: combine(
        splat(),
        simple(),
        metadata({
          fillExcept: ['logMessage', 'level', 'timestamp', 'serviceName'],
        }),
        timestamp(),
        format(myFormatJson)(),
        myPrintFormat,
      ),
    },
    consoleString: {
      level: logLevelEnv,
      json: true,
      format: combine(
        splat(),
        simple(),
        label({ label: getTraceId() }),
        timestamp(),
        format(myFormatJson)(),
        myPrintFormat,
      ),
    },
  };

  const logger = createLogger({
    level: logLevelEnv,
    defaultMeta: { serviceName },
    format: combine(
      format((info) => {
        info.logMessage = info.message;
        return info;
      })(),
    ),
    transports: [
      new transports.Console(
        process.env.LOG_JSON == 'true' || process.env.LOG_JSON === undefined
          ? options.console
          : options.consoleString,
      ),
    ],
    exitOnError: false, // do not exit on handled exceptions
  });

  logger.error = extractMessageAndParametersWrapper(logger.error);
  logger.warn = extractMessageAndParametersWrapper(logger.warn);
  logger.info = extractMessageAndParametersWrapper(logger.info);
  logger.verbose = extractMessageAndParametersWrapper(logger.verbose);
  logger.debug = extractMessageAndParametersWrapper(logger.debug);
  logger.silly = extractMessageAndParametersWrapper(logger.silly);

  // create winston to elastic transporter
  if (environment === 'dev' && elasticsearchURI) {
    const elasticsearchClient = new Elasticsearch.Client({
      node: elasticsearchURI,
    });

    const esTransportOpts = {
      level: 'silly', // decided to be silly, in order to have all logs in elk and simply filter them by kibana
      client: elasticsearchClient,
      index: 'logs',
    };

    const addWinstonElasticsearchTransporter = () => {
      logger.add(new WinstonElasticsearch(esTransportOpts));

      logger.info('added logs index into elasticsearch info:');
      logger.info('added elasticsearch transport info:');
    };

    try {
      await elasticsearchClient.indices.create({ index: 'logs' });
      addWinstonElasticsearchTransporter();
    } catch (error) {
      if (error.meta.body.error.type === 'resource_already_exists_exception') {
        addWinstonElasticsearchTransporter();
        logger.info(
          'elasticsearch logs index did not need to be created, already exists info:',
        );
      } else {
        // obviously it'll not be saved into elasticsearch
        logger.error(
          'problem on creating index of elasticsearch error: ',
          error,
        );
      }
    }
  } else {
    logger.info('no elasticserch URI provided!');
  }

  return logger;
};

module.exports = Logger;
